"use strict";

var fs        = require("fs");
var path      = require("path");
var Sequelize = require("sequelize");
var dnConfig = {
    dbname: 'passport',
    user: 'root',
    password: 'root',
    dialect: 'mysql',
    port: '3306'
};

var sequelize = new Sequelize(dnConfig.dbname, dnConfig.user, dnConfig.password);
var db        = {};

fs
    .readdirSync(__dirname)
    .filter(function(file) {
        return (file.indexOf(".") !== 0) && (file !== "index.js");
    })
    .forEach(function(file) {
        var model = sequelize.import(path.join(__dirname, file));
        db[model.name] = model;
    });

Object.keys(db).forEach(function(modelName) {
    if ("associate" in db[modelName]) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

//
// sequelize.sync().then(function() {
//     console.log('success sync')
// }).catch(function(error) {
//     // oooh, did you enter wrong database credentials?
//     console.log('fail sync',error)
// });

module.exports = db;
