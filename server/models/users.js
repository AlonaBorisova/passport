'use strict';

module.exports = function(sequelize, DataTypes){
    var users = sequelize.define('users', {
        user_name: {
            type: DataTypes.STRING,
            field: 'user_name'
        },
        bitbacket_display_name: {
            type: DataTypes.STRING,
            field: 'bitbacket_display_name'
        },
        email: {
            type: DataTypes.STRING,
            validate: {
                isEmail: true
            }
        },
        password: {
            type: DataTypes.STRING
        },
        avatar: {
            type: DataTypes.STRING,
            field: 'avatar'
        },
        bitbucket_id: {
            type: DataTypes.STRING,
            field: 'bitbacket_id'
        },
        created_bibacket_account: {
            type: DataTypes.DATE,
            field: 'created_bibacket_account'
        },
        bitbacket_type: {
            type: DataTypes.STRING,
            field: 'type'
        },
        bitbacket_repositories: {
            type: DataTypes.STRING,
            field: 'bitbacket_repositories'
        },
        bitbacket_website: {
            type: DataTypes.STRING,
            field: 'bitbacket_website'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    });
    return users;
};
