'use strict';

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const Models = require('../models');
const Password = require('../utils/password');

// initialize passport with local strategy
function init() {
    passport.use('local', new LocalStrategy({
            usernameField: 'usernameField',
            passwordField: 'passwordField'
        },
        function(usernameField, passwordField,  done) {
            process.nextTick(function () {
                // find the user in the database based on id
                Models.users.findOne({
                    where: {
                        email: usernameField
                    }
                }).then(function (user) {
                    // if the user is found, then check pass
                    if (user) {
                        //if hash is not valid catch error
                        try {
                            var passwordIsValid = Password.compare(passwordField, user.password);
                        } catch(err){
                            let error = new Error();
                            error.status = 422;
                            error.message = err;
                            return done(error);
                        }
                        //Invalid password
                        if(!passwordIsValid){
                            let error = new Error();
                            error.status = 401;
                            error.message = 'Password does not match';
                            return done(error);
                        }

                        return done(null, user); // user found, return that user
                    } else {
                        var params = {
                            email: usernameField,
                            password: Password.hash(passwordField),
                            user_name: usernameField.split('@')[0]
                        };

                        // save our user to the database
                        Models.users.create(params)
                            .then(function (newUser) {
                                // if successful, return the new user
                                return done(null, newUser);
                            }).catch(done);
                    }
                }).catch(done);

            });
        }));
}
module.exports = {
    init       : init
};
