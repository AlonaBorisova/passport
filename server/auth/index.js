'use strict';
let bitbucket = require('./bitbucket');
let local = require('./local');

module.exports = {
    bitbucket : bitbucket,
    local : local,
};
