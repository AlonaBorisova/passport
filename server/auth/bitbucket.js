'use strict';

const passport = require('passport');
const BitbucketStrategy = require('passport-bitbucket').Strategy;
const Models = require('../models');

var config = {
        consumerKey: 'JZXQsWhXpXaZvQBVUd',
        consumerSecret: 'hktT6y9SxRqWqAxkE79UwXZuVpN7qhkR',
        callbackURL: 'http://127.0.0.1:3501/auth/bitbucket/callback'
};


// initialize passport with bitbucket strategy
function init() {
    passport.use(new BitbucketStrategy({
            consumerKey: config.consumerKey,
            consumerSecret: config.consumerSecret,
            callbackURL: config.callbackURL,
        },
        function(token, tokenSecret, profile, next) {
            process.nextTick(function () {
                // find the user in the database based on their facebook id
                Models.users.findOne({
                    where: {
                        bitbucket_id: profile.id
                    }
                }).then(function (user) {
                    // if the user is found, then log them in
                    if (user) {
                        return next(null, user); // user found, return that user
                    } else {
                        var params = {
                            bitbacket_display_name: profile._json.display_name,
                            user_name: profile.username,
                            type: profile._json.type,
                            avatar: profile._json.links.avatar.href,
                            bitbucket_id: profile.id,
                            created_bibacket_account: profile._json.created_on,
                            bitbacket_repositories: profile._json.links.repositories.href,
                            bitbacket_website: profile.website

                        };


                        // save our user to the database
                        Models.users.create(params)
                            .then(function (newUser) {
                                // if successful, return the new user
                                return next(null, newUser);
                            }).catch(next);
                    }
                }).catch(next);

            });
        }));
}
module.exports = {
    init       : init
};
