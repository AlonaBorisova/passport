'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const router = require('./routes');
const passport = require('passport');
//initialize the app
const app = module.exports = express();
const session = require('express-session');

app.use(bodyParser.urlencoded({extended: true}));
app.use(session({ secret: 'hohoho',
    cookie: { secure: false },
    resave: false,
    saveUninitialized: true}));
app.use(passport.initialize());
app.use(passport.session());
app.use(router);

process.on('uncaughtException', function (err) {
    console.error((new Date).toUTCString() + ' uncaughtException:', err.message);
    console.error(err.stack);
    process.exit(1);
});



