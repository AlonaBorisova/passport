const express = require('express');
const router = express.Router();
const passport = require('passport');

const auth = require('../auth');

// initialize  passport
auth.bitbucket.init();
auth.local.init();

router.get('/', function(req, res) {
    res.render('index.ejs');
});

router.post('/auth/local', passport.authenticate('local',{session: false , failureRedirect: '/noway'}),
    function(req, res) {
        res.render('login.ejs',{
            user: req.user.dataValues
        });
    });

router.get('/auth/bitbucket', passport.authenticate('bitbucket', { session: false }));

router.get('/auth/bitbucket/callback',
    passport.authenticate('bitbucket', { session: false , failureRedirect: '/noway'}),
    function(req, res) {
        res.render('profile.ejs', {
            user: req.user.dataValues
        });

    });

router.get('/noway', function (req, res, next) {
    res.render('error.ejs');
});

router.get('/profile',  function (req, res, next) {
    res.render('profile.ejs', {
        user: req.user.dataValues
    });
});

router.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
});

module.exports = router;