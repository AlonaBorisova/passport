'use strict';
const app = require('./server');
const http = require('http');
const port = 3501;

const serverStartCallback = function () {
    console.log('info', 'Web server successfully started at port ', port);
};

let server = http.createServer(app)
    .listen(port, serverStartCallback);



